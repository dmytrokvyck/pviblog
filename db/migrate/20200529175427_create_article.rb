class CreateArticle < ActiveRecord::Migration[6.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :text
      t.belongs_to :category, null: false, foreign_key: true
    end
  end
end
