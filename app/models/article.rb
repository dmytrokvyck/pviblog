class Article < ApplicationRecord
 belongs_to :category
   has_many :comments, dependent: :destroy
belongs_to :user
   scope :ordered, -> { order(id: :desc) }
   scope :with_categories, -> { includes(:category) }


  validates :title, presence: true,
                   length: { minimum: 5 }
end
