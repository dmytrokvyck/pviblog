# PVIBlog

Basic blog developed using Ruby on Rails, devise gem and bootstrap.
Features: 

    * Add post to certain predefined category
    * Registration/login using email
    * View selected post
    * View all post of certain category
    * Edit or delete post, if you are author of it.
    * Add, comment under existing post.
    * Edit, delete comment if you are author of it.
